{
    'name': "Estate",
    'version': '1.0',
    'depends': ['base'],
    'author': "Nasif Ishtiaque Islam",
    'category': 'Category',
    'description': """
    Description text
    """,
    'data': [
        'views/estate_menus.xml',
        'views/estate_property_views.xml',
        'views/estate_property_tag_views.xml',
        'views/estate_property_type_views.xml',
        'views/estate_property_offer_views.xml',
        'security/ir.model.access.csv',
    ],
    'application': True,
    'license': 'LGPL-3',
}


