import datetime
from odoo import fields, models, api
from dateutil.relativedelta import relativedelta
from odoo.exceptions import AccessError, UserError, ValidationError

from datetime import timedelta


class EstatePropertyOffer(models.Model):
    _name = "estate.property.offer"
    _description = "estate property offers"

    price = fields.Float()
    status = fields.Selection(
        string='Status',
        selection=[('accepted', 'Accepted'), ('refused', 'Refused'), ]
    )

    partner_id = fields.Many2one("res.partner", required=True)
    property_id = fields.Many2one("estate.property", required=True)
    validity = fields.Integer(default=7)

    date_deadline = fields.Date(compute="_compute_deadline", inverse='_deadline_set')

    @api.depends('validity')
    def _compute_deadline(self):
        for record in self:
            record.date_deadline = fields.Date.today() + relativedelta(days=record.validity)

    def _deadline_set(self):
        for record in self:
            record.validity = max((record.date_deadline - fields.Date.today()).days, 0)

    def action_do_accept(self):
        for record in self:
            if record.property_id.expected_price * .9 < record.price:
                record.status = "accepted"
                record.property_id.buyer_id = record.partner_id
                record.property_id.selling_price = record.price
            else:
                raise ValidationError('Selling price cannot be lower than 90% of the expected price.')

        return True

    def action_do_refuse(self):
        for record in self:
            record.status = "refused"
        return True

    @api.constrains('price')
    def _check_description(self):
        for record in self:
            if record.price < 0:
                raise ValidationError("Offer price must be greater than zero ! ")

