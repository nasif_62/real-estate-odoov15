# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api
from dateutil.relativedelta import relativedelta
from odoo.exceptions import AccessError, UserError, ValidationError


class EstateProperty(models.Model):
    _name = "estate.property"
    _description = "estate property details"

    name = fields.Char(required=True)
    description = fields.Text()
    postcode = fields.Char()
    available_from = fields.Date(default=lambda self: fields.Datetime.today() + relativedelta(months=3), copy=False)
    expected_price = fields.Float()
    selling_price = fields.Float(readonly=True, copy=False)
    bedrooms = fields.Integer(default=2)
    facades = fields.Integer()
    garage = fields.Boolean()
    garden = fields.Boolean()

    garden_orientation = fields.Selection(
        string='Garden Orientation',
        selection=[('north', 'North'), ('south', 'South'), ('east', 'East'), ('west', 'West')]
    )
    status = fields.Selection(
        string='Status',
        selection=[('new', 'New'), ('offer_received', 'Offer Received'), ('offer_accepted', 'Offer Accepted'), ('sold', 'Sold'), ('cancelled', 'Cancelled'), ],
        default='new'
    )
    active = fields.Boolean(default=True)
    property_type_id = fields.Many2one("estate.property.type", string="Property Type")
    seller_id = fields.Many2one("res.users", string="Salesman", default=lambda self: self.env.user)
    buyer_id = fields.Many2one("res.partner", string="Buyer", copy=False)

    tag_ids = fields.Many2many("estate.property.tag", string="Property Tag")

    total_area = fields.Float(compute="_total_area")
    garden_area = fields.Integer()
    living_area = fields.Integer()

    @api.depends("living_area", "garden_area")
    def _total_area(self):
        for record in self:
            record.total_area = record.living_area + record.garden_area

    best_offer = fields.Float(compute="_best_offer")
    offer_ids = fields.One2many("estate.property.offer", "property_id", string="Offers")

    @api.depends("offer_ids")
    def _best_offer(self):
        for record in self:
            record.best_offer = max(record.mapped('offer_ids.price'), default=0)

    @api.onchange("garden")
    def _onchange_garden_info(self):
        if self.garden:
            self.garden_area = 10
            self.garden_orientation = "north"
        else:
            self.garden_area = 0
            self.garden_orientation = False

    def action_do_cancel(self):
        for record in self:
            if record.status == 'sold':
                raise UserError("Sold properties cannot be cancelled.")
            record.status = "cancelled"
        return True

    def action_do_sold(self):
        for record in self:
            if record.status == 'cancelled':
                raise UserError("Cancelled properties cannot be sold.")
            record.status = "sold"
        return True

    _sql_constraints = [
        ('expected_price_positive', 'CHECK (expected_price >= 0)', 'The expected price must be positive !'),
        ('selling_price_positive', 'CHECK (selling_price >= 0)', 'The selling price must be positive !'),
        ('property_tag_unique', 'unique (property_tag,property_type)', 'The property tag and property type must be unique !')
    ]
